#!/bin/bash

CURRENT_PATH=`pwd -P`
REPOSITORY=$CURRENT_PATH
PROJECT_NAME=media-server

echo "> Check Current Running Application PID"

CURRENT_PID=$(pgrep -f ${PROJECT_NAME})
# pgrep -> process id만 추출하는 명령어
# -f 프로세스 이름으로 찾음

echo "Current Running Application PID: $CURRENT_PID"

if [ -z "$CURRENT_PID" ]; then
        echo "> Not exit because don't exist current running application"
else
        echo "> kill -15 $CURRENT_PID"
        kill -15 $CURRENT_PID
        sleep 5
fi

echo "> Deploy New Application"

JAR_NAME=$(ls -tr $REPOSITORY | grep .jar | tail -n 1)
# 새로 실행할 jar파일명을 찾음
# 여러 jar 파일이 생기기 때문에 tail -n으로 가장 나중의 jar 파일(최신파일)을 변수에 저장

echo "> JAR Name: $JAR_NAME"

nohup java -jar \
-Dspring.config.location=/home/meloning/maum-media-server/application-prod.yml \
$REPOSITORY/$JAR_NAME 2>&1 &
