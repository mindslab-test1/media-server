#!/usr/bin/env bash

PROTOCOL=$1
DOMAIN=$2

# 현재 stop.sh가 속해 있는 경로를 찾음
ABSPATH=$(readlink -f "$0")
ABSDIR=$(dirname "$ABSPATH")

# shellcheck disable=SC1090
# 자바로 보면 일종의 import 구문
# 해당 코드로 인해 stop.sh에서도 profile.sh의 여러 function을 사용할 수 있음.
source "${ABSDIR}"/profile.sh "$PROTOCOL" "$DOMAIN"

IDLE_PROFILE=$(find_idle_profile)
IDLE_PORT=$(find_idle_port)

MINDS_DOCKER_REGISTRY=$3
FILE_STORAGE_VOLUME=$4
LOG_VOLUME=$5
GIT_COMMIT=$6
SPRING_PROFILES_ACTIVE=$7

echo ">profile=$IDLE_PROFILE 로 docker 실행"
echo "> docker run -d --user $(id -u):$(id -g) -p $IDLE_PORT:8950 -e PROFILE=$IDLE_PROFILE -e SPRING_PROFILES_ACTIVE=$SPRING_PROFILES_ACTIVE -v /etc/localtime:/etc/localtime:ro -v /usr/share/zoneinfo/Asia/Seoul:/etc/timezone:ro -v $FILE_STORAGE_VOLUME -v $LOG_VOLUME --name media-server-$IDLE_PROFILE ${MINDS_DOCKER_REGISTRY}/media-server:${GIT_COMMIT}"
docker run -d --user $(id -u):$(id -g) -p "$IDLE_PORT":8950 -e PROFILE="$IDLE_PROFILE" -e SPRING_PROFILES_ACTIVE="$SPRING_PROFILES_ACTIVE" -v /etc/localtime:/etc/localtime:ro -v /usr/share/zoneinfo/Asia/Seoul:/etc/timezone:ro -v "$FILE_STORAGE_VOLUME" -v "$LOG_VOLUME" --name media-server-"$IDLE_PROFILE" "${MINDS_DOCKER_REGISTRY}"/media-server:"${GIT_COMMIT}"

echo ">profile=backup으로 docker 실행"
echo "> docker run -d --user $(id -u):$(id -g) -p 8952:8950 -e PROFILE=backup -e SPRING_PROFILES_ACTIVE=$SPRING_PROFILES_ACTIVE -v /etc/localtime:/etc/localtime:ro -v /usr/share/zoneinfo/Asia/Seoul:/etc/timezone:ro -v $FILE_STORAGE_VOLUME -v $LOG_VOLUME --name media-server-backup ${MINDS_DOCKER_REGISTRY}/media-server:${GIT_COMMIT}"
docker run -d --user $(id -u):$(id -g) -p 8952:8950 -e PROFILE="$IDLE_PROFILE" -e SPRING_PROFILES_ACTIVE="$SPRING_PROFILES_ACTIVE" -v /etc/localtime:/etc/localtime:ro -v /usr/share/zoneinfo/Asia/Seoul:/etc/timezone:ro -v "$FILE_STORAGE_VOLUME" -v "$LOG_VOLUME" --name media-server-backup "${MINDS_DOCKER_REGISTRY}"/media-server:"${GIT_COMMIT}"