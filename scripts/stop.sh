#!/usr/bin/env bash

# 현재 stop.sh가 속해 있는 경로를 찾음
ABSPATH=$(readlink -f "$0")
ABSDIR=$(dirname "$ABSPATH")

PROTOCOL=$1
DOMAIN=$2

# shellcheck disable=SC1090
# 자바로 보면 일종의 import 구문
# 해당 코드로 인해 stop.sh에서도 profile.sh의 여러 function을 사용할 수 있음.
source "${ABSDIR}"/profile.sh "$PROTOCOL" "$DOMAIN"

IDLE_PORT=$(find_idle_port)
IDLE_PROFILE=$(find_idle_profile)

echo "> $IDLE_PROFILE docker container id 확인"
IDLE_CONTAINER_ID=$(docker ps -aqf "name=media-server-$IDLE_PROFILE")

if [ -z "${IDLE_CONTAINER_ID}" ]
then
  echo "> 현재 구동 중인 Docker Container가 없으므로 종료하지 않습니다."
else
  echo "> docker rm -f $IDLE_CONTAINER_ID"
  echo "> docker rm -f media-server-backup"
  docker rm -f "${IDLE_CONTAINER_ID}"
  docker rm -f media-server-backup
  sleep 50
fi
