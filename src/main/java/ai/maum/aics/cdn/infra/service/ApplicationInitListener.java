package ai.maum.aics.cdn.infra.service;

import ai.maum.aics.cdn.infra.config.ExternalProfileProperties;
import ai.maum.aics.cdn.infra.config.UploadFileProperties;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class ApplicationInitListener implements CommandLineRunner {
    private final UploadFileProperties uploadFileProperties;
    private final ExternalProfileProperties externalProfileProperties;

    private static Logger logger = LoggerFactory.getLogger(ApplicationInitListener.class);

    @Override
    public void run(String... args) throws Exception {
        logger.info("Check upload directory validation '" + uploadFileProperties.getUploadDir() + "'");
        if(uploadFileProperties.checkUploadDir()) {
            logger.info("UploadDirectory is no problem. Please use the media-server function");
        }
        logger.info("Initial External Profile: " + externalProfileProperties.getExternalProfile());
    }
}
