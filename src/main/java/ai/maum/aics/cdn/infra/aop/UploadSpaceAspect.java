package ai.maum.aics.cdn.infra.aop;

import ai.maum.aics.cdn.infra.config.UploadFileProperties;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
@Aspect
public class UploadSpaceAspect {

    private final UploadFileProperties uploadFileProperties;

    @Around("@annotation(ai.maum.aics.cdn.infra.aop.CheckUploadSpace) && execution(public * *(..))")
    public Object checkUploadSpace(final ProceedingJoinPoint joinPoint) throws Throwable {
        uploadFileProperties.checkUploadDirSpace();

        Object proceed = joinPoint.proceed();

        uploadFileProperties.checkUploadDirSpaceStepByStep();

        return proceed;
    }
}
