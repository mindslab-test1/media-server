package ai.maum.aics.cdn.infra.config;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@RequiredArgsConstructor
@Configuration
public class ExternalProfileProperties {

    private static final Logger logger = LoggerFactory.getLogger(ExternalProfileProperties.class);

    @Value("${profile:real1}")
    private String profile;

    public String getExternalProfile() {
        return profile;
    }
}
