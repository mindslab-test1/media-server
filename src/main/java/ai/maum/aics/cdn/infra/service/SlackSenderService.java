package ai.maum.aics.cdn.infra.service;

import ai.maum.aics.cdn.infra.config.NotificationSlackProperties;
import lombok.RequiredArgsConstructor;
import net.gpedro.integrations.slack.SlackApi;
import net.gpedro.integrations.slack.SlackAttachment;
import net.gpedro.integrations.slack.SlackField;
import net.gpedro.integrations.slack.SlackMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Service
public class SlackSenderService {
    private static final Logger logger = LoggerFactory.getLogger(SlackSenderService.class);

    private final NotificationSlackProperties notificationSlackProperties;

    @Async("notificationThreadExecutor")
    public void send(NotificationSlackProperties.Recipient recipient, String content, String color) {
        // TODO: Template format 만들어서 switch로 분기후 받아 처리할것 (message format 변경 가능성 높음 / will use Factory)
        SlackApi slackApi = new SlackApi(recipient.getIncomingWebhookUrl());
        List<SlackField> fields = new ArrayList<>();

        SlackField message = new SlackField();
        message.setTitle("Message");
        message.setValue(content);
        message.setShorten(false);
        fields.add(message);

        SlackAttachment slackAttachment = new SlackAttachment();
        slackAttachment.setFallback(content);
        slackAttachment.setColor(color);
        slackAttachment.setFields(fields);

        SlackMessage slackMessage = new SlackMessage("");
        slackMessage.setChannel(recipient.getChannel());
        slackMessage.setUsername(notificationSlackProperties.getUsername());
        slackMessage.setIcon(notificationSlackProperties.getIcon());
        slackMessage.setAttachments(Collections.singletonList(slackAttachment));

        slackApi.call(slackMessage);
        logger.info(Thread.currentThread().getName() + " - Send Slack Success for " + recipient.getChannel());
    }
}
