package ai.maum.aics.cdn.infra.service;

import ai.maum.aics.cdn.infra.config.NotificationSlackProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class NotificationService {

    private final NotificationSlackProperties notificationSlackProperties;
    private final SlackSenderService slackSenderService;

    public void sendNotification(final String message, final String color) {
        if (notificationSlackProperties.isEnabled()) {
            notificationSlackProperties.getRecipients().forEach(recipient -> {
                slackSenderService.send(recipient, message, color);
            });
        }
    }
}
