package ai.maum.aics.cdn.infra.service;

import ai.maum.aics.cdn.core.exceptions.MediaServerException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

@RequiredArgsConstructor
@Service
public class FileHandleService {

    private static final Logger logger = LoggerFactory.getLogger(FileHandleService.class);

    @Async("fileIOThreadExecutor")
    public void copyFileToDirectoryAsync(MultipartFile file, File destination) {
        try {
            FileUtils.copyFileToDirectory(file.getResource().getFile(), destination);
            logger.info(Thread.currentThread().getName() + " - [Success] " + file.getOriginalFilename() + " to " + destination.getPath());
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
            try {
                FileUtils.forceDelete(destination);
            } catch (IOException e) {
                logger.error(ex.getLocalizedMessage());
                throw new MediaServerException("Could not remove file (" + e.getLocalizedMessage() + ")");
            }
            throw new MediaServerException("Could not store file (" + ex.getLocalizedMessage() + ")");
        }
    }

    @Async("fileIOThreadExecutor")
    public void copyToDirectory(List<File> files, File destination) {
        try {
            FileUtils.copyToDirectory(files, destination);
            logger.info(Thread.currentThread().getName() + " - [Success] " + files.toString() + " to " + destination.getPath());
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
            try {
                FileUtils.forceDelete(destination);
            } catch (IOException e) {
                logger.error(ex.getLocalizedMessage());
                throw new MediaServerException("Could not remove file (" + e.getLocalizedMessage() + ")");
            }
            throw new MediaServerException("Could not store file (" + ex.getLocalizedMessage() + ")");
        }
    }


}
