package ai.maum.aics.cdn.infra.service;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class ApplicationShutdownListener implements ApplicationListener<ContextClosedEvent> {
    private final NotificationService notificationService;

    private static final Logger logger = LoggerFactory.getLogger(ApplicationShutdownListener.class);

    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        logger.error("Shutdown Event Triggering");

        // Notification Default (Email), Slack
        // TODO: 무중단 배포하게되면 계속 꺼질때마다 알람올텐데...?
        //          - 이렇게 구현하는 방향이 맞는지 체크
        //          - 방향이 맞다면 무중단 배포로 죽을시 환경변수를 따로 받아 분기처리 고민해볼것.
        notificationService.sendNotification("Media Server is died. Please check log", "danger");
    }
}
