package ai.maum.aics.cdn.infra.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "notification.slack")
public class NotificationSlackProperties {

    private boolean enabled;
    private String username;
    private String icon;
    private List<Recipient> recipients;

    @Getter
    @Setter
    public static class Recipient {
        private String channel;
        private String incomingWebhookUrl;
    }

}
