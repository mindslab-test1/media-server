package ai.maum.aics.cdn.infra.config;

import ai.maum.aics.cdn.core.exceptions.DirectorySpaceOverflowException;
import ai.maum.aics.cdn.core.exceptions.NotDirectoryException;
import ai.maum.aics.cdn.core.exceptions.NotExistDirectoryException;
import ai.maum.aics.cdn.infra.service.NotificationService;
import ai.maum.aics.cdn.util.FileUtils;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.apache.commons.io.FileUtils.ONE_GB;


@Getter
@RequiredArgsConstructor
@Configuration
public class UploadFileProperties {

    private final NotificationService notificationService;

    private static final Logger logger = LoggerFactory.getLogger(UploadFileProperties.class);

    @Value("${file.upload-dir:/home/junsu/media}")
    private String uploadDir;

    @Value("${file.chunk-size:8}")
    private Long chunkSize;

    private AtomicBoolean locked = new AtomicBoolean(false);

    public boolean checkUploadDir() throws IOException {
        File uploadDirectory = new File(uploadDir);
        if (!uploadDirectory.exists()) {
            throw new NotExistDirectoryException(HttpStatus.INTERNAL_SERVER_ERROR, "Not Exist directory '" + uploadDirectory.getPath() + "'");
        }
        if (!uploadDirectory.isDirectory()) {
            throw new NotDirectoryException(HttpStatus.INTERNAL_SERVER_ERROR, "Not directory '" + uploadDirectory.getPath() + "'");
        }
        if (uploadDirectory.isHidden()) {
            throw new NotDirectoryException(HttpStatus.INTERNAL_SERVER_ERROR, "This directory is hidden dir '" + uploadDirectory.getPath() + "'");
        }
        if (!uploadDirectory.canRead()) {
            logger.error("Not read '" + uploadDirectory.getPath() + "' " +
                    "Please check chmod, chown(The current owner is '" + Files.getOwner(uploadDirectory.toPath()).getName() + "')");
            return false;
        }
        if (!uploadDirectory.canWrite()) {
            logger.error("Not write '" + uploadDirectory.getPath() + "' " +
                    "Please check chmod, chown(The current owner is '" + Files.getOwner(uploadDirectory.toPath()).getName() + "')");
            return false;
        }
        return true;
    }

    public void checkUploadDirSpace() {
        File directory = new File(uploadDir);
        if (directory.getUsableSpace() <= ONE_GB) {
            String totalSpaceWithUnit = FileUtils.readableFileSizeUnit(directory.getTotalSpace());
            String usedSpaceWithUnit = FileUtils.readableFileSizeUnit(directory.getTotalSpace()-directory.getUsableSpace());
            String displaySpaceFormat = usedSpaceWithUnit + " / " + totalSpaceWithUnit;

            logger.error("There is not enough hard disk working space. " + displaySpaceFormat);

            throw new DirectorySpaceOverflowException(HttpStatus.BAD_REQUEST, "There is not enough hard disk working space. " + displaySpaceFormat);
        }
    }

    public void checkUploadDirSpaceStepByStep() {
        File directory = new File(uploadDir);
        if (directory.getUsableSpace() <= directory.getTotalSpace()/5) {
            String totalSpaceWithUnit = FileUtils.readableFileSizeUnit(directory.getTotalSpace());
            String usedSpaceWithUnit = FileUtils.readableFileSizeUnit(directory.getTotalSpace()-directory.getUsableSpace());
            String displaySpaceFormat = usedSpaceWithUnit + " / " + totalSpaceWithUnit;

            logger.warn("About 20% of the upload space remains. " + displaySpaceFormat);

            if (locked.compareAndSet(false, true)) notificationService.sendNotification("About 20% of the upload space remains. " + displaySpaceFormat, "warning");
        } else if (directory.getUsableSpace() <= directory.getTotalSpace()/2) {
            String totalSpaceWithUnit = FileUtils.readableFileSizeUnit(directory.getTotalSpace());
            String usedSpaceWithUnit = FileUtils.readableFileSizeUnit(directory.getTotalSpace()-directory.getUsableSpace());
            String displaySpaceFormat = usedSpaceWithUnit + " / " + totalSpaceWithUnit;

            logger.warn("About half of the upload space remains. " + displaySpaceFormat);

            if (locked.compareAndSet(false, true)) notificationService.sendNotification("About half of the upload space remains. " + displaySpaceFormat, "warning");
        }
    }

}

