package ai.maum.aics.cdn.infra.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

@Configuration
@EnableAsync(proxyTargetClass = true)
public class AsyncConfig implements AsyncConfigurer {

    @Value("${async.queue-capacity:200}")
    private int QUEUE_CAPACITY;

    @Bean("notificationThreadExecutor")
    public Executor getAsyncNotificationExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(Runtime.getRuntime().availableProcessors());
        threadPoolTaskExecutor.setMaxPoolSize(Runtime.getRuntime().availableProcessors());
        threadPoolTaskExecutor.setQueueCapacity(QUEUE_CAPACITY);
        threadPoolTaskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        threadPoolTaskExecutor.setThreadNamePrefix("NotificationThreadExecutor");
        threadPoolTaskExecutor.initialize();
        return threadPoolTaskExecutor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        // TODO: Exception Handling 되는지 테스트 해볼것
        //          - Policy적용시 로그가 남지 않고 fallback되어 처리됨.
        //          - Log가 남지 않기에 Queue Size가 초과되었는지 알수 없음.
        //          - log로 남긴 후, Fallback 처리되도록 구성 및 테스트 필요
        //          - 남긴 로그를 토대로 모니터링에서 확인하여 처리(QueueSize 늘리는등)할지, 알림을 주어 처리할지는 추후 생각해볼 사항
        return new AsyncExceptionHandler();
    }

    static class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

        private static final Logger logger = LoggerFactory.getLogger(AsyncExceptionHandler.class);

        @Override
        public void handleUncaughtException(Throwable ex, Method method, Object... params) {
            logger.error(generateThreadDump());
        }

        private String generateThreadDump() {
            StringBuilder dump = new StringBuilder();
            ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
            ThreadInfo[] threadInfos = threadMXBean.getThreadInfo(threadMXBean.getAllThreadIds(), 100);
            Arrays.stream(threadInfos).forEach(threadInfo -> {
                dump.append('"');
                dump.append(threadInfo.getThreadName());
                dump.append("\" ");
                Thread.State state = threadInfo.getThreadState();
                dump.append("\n java.lang.Thread.State: ");
                dump.append(state);
                StackTraceElement[] stackTraceElements = threadInfo.getStackTrace();
                Arrays.stream(stackTraceElements).forEach(stackTraceElement -> {
                    dump.append("\n        at ");
                    dump.append(stackTraceElement);
                });
                dump.append("\n\n");
            });
            return dump.toString();
        }
    }
}
