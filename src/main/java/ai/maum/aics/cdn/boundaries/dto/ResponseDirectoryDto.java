package ai.maum.aics.cdn.boundaries.dto;

import lombok.*;


@Getter
@Setter
@AllArgsConstructor
public class ResponseDirectoryDto {
    private String directoryPath;
    private String updatedDirectoryName;
}
