package ai.maum.aics.cdn.boundaries.custom.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import uap_clj.java.api.Browser;
import uap_clj.java.api.Device;
import uap_clj.java.api.OS;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Component
public class CommonInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(CommonInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String url = request.getRequestURL().toString();

        String methodName = request.getMethod();

        String queryString = request.getQueryString();
        if (queryString != null) {
            url = url + "?" + queryString;
        }
        logger.info(String.format("[%s] %s", methodName, url));

        Map<String, String[]> params = request.getParameterMap();
        for (String key : params.keySet()) {
            String[] value = params.get(key);
            logger.info(String.format("(p) <-- %s = %s", key, String.join(",", value)));
        }

        String requestUserAgent = request.getHeader("User-Agent");
        Map<String, String> browser = Browser.lookup(requestUserAgent);
        Map<String, String> os = (Map<String, String>) OS.lookup(requestUserAgent);
        Map<String, String> device = (Map<String, String>) Device.lookup(requestUserAgent);

        String browserStr = String.format("- Browser: {family=%s, patch=%s, major=%s, minor=%s}",
                browser.get("family"), browser.get("patch"), browser.get("major"), browser.get("minor"));

        String osStr = String.format("- OS: {family=%s, patch=%s, patch_minor=%s, major=%s, minor=%s}",
                os.get("family"), os.get("patch"), os.get("patch_minor"), os.get("major"), os.get("minor"));

        String deviceStr = String.format("- Device: {family=%s, model=%s, brand=%s}",
                device.get("family"), device.get("model"), device.get("brand"));

        logger.info(requestUserAgent);

        return super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
    }

    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        super.afterConcurrentHandlingStarted(request, response, handler);
    }
}
