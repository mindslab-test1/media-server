package ai.maum.aics.cdn.boundaries.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
public class ResponseUpdateDirectoryDto {
    private String directoryPath;
    private String updatedDirectoryName;
}
