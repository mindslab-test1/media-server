package ai.maum.aics.cdn.boundaries.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@NoArgsConstructor
public class RequestDeleteDirectoryDto {
    @NotBlank
    @Pattern(regexp = "^/$|^(/[\\w-]+)+$", message = "Please check request param.")
    private String directoryNameWithPath;
}
