package ai.maum.aics.cdn.boundaries.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class RequestGetContentsPathDto {
    @NotBlank
    @Pattern(regexp = "^/$|^(/[\\w-]+)+$", message = "Please check request param. Invalid param")
    private String uploadDirectory;

    @Size(min = 1, message = "Please check request param. There must be at least one element.")
    private List<@Pattern(regexp = "^[가-힣\\w-.,!?()\\s]+\\.[A-Za-z0-9]{3,4}$", message = "Please check request param. It's not the right filename.") String> filenames;
}
