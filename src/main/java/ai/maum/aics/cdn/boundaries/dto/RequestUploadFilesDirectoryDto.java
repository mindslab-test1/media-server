package ai.maum.aics.cdn.boundaries.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class RequestUploadFilesDirectoryDto {
    @Size(min = 1, message = "Please check request param. There must be at least one file.")
    private List<MultipartFile> files;

    @NotBlank
    @Pattern(regexp = "^/$|^(/[\\w_-]+)+$", message = "Please check request param. Invalid param")
    private String uploadDirectory;
}
