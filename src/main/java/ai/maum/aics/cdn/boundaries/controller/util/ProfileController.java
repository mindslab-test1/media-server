package ai.maum.aics.cdn.boundaries.controller.util;

import ai.maum.aics.cdn.infra.config.ExternalProfileProperties;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class ProfileController {
    private final ExternalProfileProperties externalProfileProperties;

    private static final Logger logger = LoggerFactory.getLogger(ProfileController.class);

    @GetMapping("/media/profile")
    public String profile() {
        String externalProfile = externalProfileProperties.getExternalProfile();
        logger.info("External profile: " + externalProfile);
        return externalProfile;
    }
}
