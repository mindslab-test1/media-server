package ai.maum.aics.cdn.boundaries.custom.annotation;

import org.springframework.core.MethodParameter;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Map;

public class WildcardPathVariableHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {

    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.hasParameterAnnotation(WildcardPathVariable.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest nativeWebRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) nativeWebRequest.getNativeRequest();

        String pattern = (String) httpServletRequest.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);
        String path = (String) httpServletRequest.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        Map<String, String> pathVariableMap = antPathMatcher.extractUriTemplateVariables(pattern, path);
        String filename = pathVariableMap.get("filename");

        String[] wildcardFullPathArray = antPathMatcher.extractPathWithinPattern(pattern, path).split(File.separator + filename);

        // Case When the request file exists in the root directory('upload-dir')
        if (wildcardFullPathArray[0].contains(filename)) {
            return "";
        }

        return wildcardFullPathArray[0];
    }
}
