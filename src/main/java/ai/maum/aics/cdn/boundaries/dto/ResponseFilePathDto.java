package ai.maum.aics.cdn.boundaries.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class ResponseFilePathDto {
    private String fullPathUrl;
    private String pathUrl;
}
