package ai.maum.aics.cdn.boundaries.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class RequestGetContentPathDto {
    @NotBlank
    @Pattern(regexp = "^/$|^(/[\\w-]+)+$", message = "Please check request param. Invalid param")
    private String uploadDirectory;

    @NotBlank
    @Pattern(regexp = "^[가-힣\\w-.,!?()\\s]+\\.[A-Za-z0-9]{3,4}$", message = "Please check request param. It's not the right filename.")
    private String filename;
}
