package ai.maum.aics.cdn.boundaries.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.nio.file.Path;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class ResponseFileInfoDto {
    private final String name;
    private final String path;
    private final long size;
    private final boolean isDirectory;
    private final boolean isFile;
    private final String parentPath;
}
