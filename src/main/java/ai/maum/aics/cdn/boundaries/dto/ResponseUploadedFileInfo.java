package ai.maum.aics.cdn.boundaries.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseUploadedFileInfo {
    private String filename;
    private String fileDownloadUri;
    private String fileType;
    private long fileSize;
}
