package ai.maum.aics.cdn.boundaries.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@NoArgsConstructor
public class RequestRemoveContentDto {
    @NotBlank
    @Pattern(regexp = "^/$|^(/[\\w-]+)+/[가-힣\\w-.,!?()\\s]+\\.[A-Za-z0-9]{3,4}$", message = "Please check request param. Invalid param")
    private String deleteFileFullPath;
}
