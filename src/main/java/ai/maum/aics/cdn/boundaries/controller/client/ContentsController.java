package ai.maum.aics.cdn.boundaries.controller.client;

import ai.maum.aics.cdn.boundaries.custom.annotation.WildcardPathVariable;
import ai.maum.aics.cdn.boundaries.dto.*;
import ai.maum.aics.cdn.core.usecases.contents.GetContentsPathUsecase;
import ai.maum.aics.cdn.core.usecases.contents.LoadContentsUsecase;
import ai.maum.aics.cdn.core.usecases.contents.RemoveContentsUsecase;
import ai.maum.aics.cdn.core.usecases.contents.StoreContentsUsecase;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourceRegion;
import org.springframework.http.*;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/media")
public class ContentsController {
    private final StoreContentsUsecase storeContentsUsecase;
    private final LoadContentsUsecase loadContentsUsecase;
    private final RemoveContentsUsecase removeContentsUsecase;
    private final GetContentsPathUsecase getContentsPathUsecase;

    private static final Logger logger = LoggerFactory.getLogger(ContentsController.class);

    @PostMapping("/file:upload")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseUploadedFileInfo uploadFile(@Valid RequestUploadDirectoryDto requestUploadDirectoryDto,
                                               BindingResult bindingResult) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        return storeContentsUsecase.execute(requestUploadDirectoryDto);
    }

    @PostMapping("/files:upload")
    @ResponseStatus(HttpStatus.CREATED)
    public List<ResponseUploadedFileInfo> uploadFiles(@Valid RequestUploadFilesDirectoryDto requestUploadFilesDirectoryDto,
                                                      BindingResult bindingResult) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        return storeContentsUsecase.execute(requestUploadFilesDirectoryDto);
    }

    @PostMapping("/file/path:get")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseFilePathDto getFilePath(@RequestBody @Valid RequestGetContentPathDto requestGetContentPathDto,
                              BindingResult bindingResult) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        return getContentsPathUsecase.execute(requestGetContentPathDto);
    }

    @PostMapping("/files/path:get")
    @ResponseStatus(HttpStatus.CREATED)
    public List<ResponseFilePathDto> getFilePaths(@RequestBody @Valid RequestGetContentsPathDto requestGetContentsPathDto,
                                     BindingResult bindingResult) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        return getContentsPathUsecase.execute(requestGetContentsPathDto);
    }

    @GetMapping("/**/{filename:.+}")
    public ResponseEntity<Object> downloadFile(@WildcardPathVariable String dirPath,
                                               @PathVariable String filename,
                                               @RequestHeader HttpHeaders httpHeaders) throws IOException {
        Pair<Resource, ResourceRegion> resourcePair = loadContentsUsecase.execute(dirPath, filename, httpHeaders);
        Resource resource = resourcePair.getLeft();
        ResourceRegion resourceRegion = resourcePair.getRight();

        if (resource == null) {
            return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT)
                    .contentType(MediaTypeFactory.getMediaType(filename).orElse(MediaType.APPLICATION_OCTET_STREAM))
                    .body(resourceRegion);
        }

        return ResponseEntity.ok()
                .contentType(MediaTypeFactory.getMediaType(filename).orElse(MediaType.APPLICATION_OCTET_STREAM))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @DeleteMapping("/file:delete")
    public ResponseEntity<Object> removeFile(@RequestBody RequestRemoveContentDto requestRemoveContentDto,
                                             BindingResult bindingResult) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        removeContentsUsecase.execute(requestRemoveContentDto);
        return ResponseEntity.noContent().build();
    }
}
