package ai.maum.aics.cdn.boundaries.controller.admin;

import ai.maum.aics.cdn.boundaries.dto.*;
import ai.maum.aics.cdn.core.usecases.directory.CreateDirectoryUsecase;
import ai.maum.aics.cdn.core.usecases.directory.DeleteDirectoryUsecase;
import ai.maum.aics.cdn.core.usecases.directory.ListDirectoryUsecase;
import ai.maum.aics.cdn.core.usecases.directory.UpdateDirectoryUsecase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/media/admin")
public class DirectoryApiController {

    private final CreateDirectoryUsecase createDirectoryUsecase;
    private final UpdateDirectoryUsecase updateDirectoryUsecase;
    private final ListDirectoryUsecase listDirectoryUsecase;
    private final DeleteDirectoryUsecase deleteDirectoryUsecase;

    @GetMapping("/directory/list")
    public ResponseEntity<List<ResponseFileInfoDto>> listDir(@RequestParam(value = "path", required = false, defaultValue = "/") String path) {
        return new ResponseEntity<>(listDirectoryUsecase.execute(path), HttpStatus.OK);
    }

    @PostMapping("/directory")
    public ResponseEntity createDir(@RequestBody @Valid RequestDirectoryDto requestDirectoryDto,
                                    BindingResult bindingResult) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        createDirectoryUsecase.execute(requestDirectoryDto);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/directory/{directoryName:.+}")
    public ResponseDirectoryDto updateDir(@PathVariable String directoryName,
                                          @RequestBody @Valid RequestUpdateDirectoryDto requestUpdateDirectoryDto,
                                          BindingResult bindingResult) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        return updateDirectoryUsecase.execute(directoryName, requestUpdateDirectoryDto);
    }

    @DeleteMapping("/directory")
    public ResponseEntity deleteDir(@RequestBody @Valid RequestDeleteDirectoryDto requestDeleteDirectoryDto,
                                    BindingResult bindingResult) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        deleteDirectoryUsecase.execute(requestDeleteDirectoryDto);
        return ResponseEntity.noContent().build();
    }
}
