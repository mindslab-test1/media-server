package ai.maum.aics.cdn.core.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
public class UnsupportedContentTypeException extends BaseException {
    private final HttpStatus statusCode;
    private final String message;

    public UnsupportedContentTypeException(HttpStatus statusCode, String message) {
        super(statusCode, "unsupported_content_type", message);
        this.statusCode = statusCode;
        this.message = message;
    }
}
