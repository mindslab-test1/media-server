package ai.maum.aics.cdn.core.usecases;

import ai.maum.aics.cdn.core.exceptions.InvalidParamRequestException;
import ai.maum.aics.cdn.core.exceptions.NotFoundRequestFileException;
import ai.maum.aics.cdn.core.models.ContentType;
import ai.maum.aics.cdn.util.FileUtils;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Path;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Pattern;

@RequiredArgsConstructor
@Component
public class MultipartFileHelper {
    private final PathProvider pathProvider;
    private final FileHandler fileHandler;

    private static final Logger logger = LoggerFactory.getLogger(MultipartFileHelper.class);

    private String checkUploadFilename(MultipartFile file) {
        // Normalize file name
        Optional<String> optionalFilename = Optional.ofNullable(file.getOriginalFilename());
        if (!optionalFilename.isPresent()) {
            logger.error("The original name in the MultipartFile Object is null.");
            throw new NotFoundRequestFileException(HttpStatus.NOT_FOUND, "Not exist filename");
        }
        String filename = optionalFilename.get();

        // Check if the file's name contains invalid characters
        return checkFilename(filename);
    }

    private String checkFilename(String filename) {
        // Check if the file's name contains invalid characters
        if (!Pattern.matches("^[가-힣\\w-.,!?()\\s]+\\.[A-Za-z0-9]{3,4}$", filename)) {
            throw new InvalidParamRequestException(HttpStatus.BAD_REQUEST, "Sorry! Filename contains invalid values :" + filename);
        }

        // Handle duplicated filename for UUID
        return getUUID() + "_" + filename;
    }

    private String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public String createFile(MultipartFile file, String uploadDirectory) {
        logger.info("Request upload file info(" + file.getContentType() + ", " + file.getSize() + " -> " + FileUtils.readableFileSizeUnit(file.getSize()) + ")");
        String directoryPathWithGroupName = pathProvider.getUploadDirectoryWithGroupName(file.getOriginalFilename(), uploadDirectory);
        String filenameWithUploadDirectory;
        if (ContentType.getExtensionTypeByFilename(uploadDirectory).isEmpty()) {
            filenameWithUploadDirectory = "";
        } else {
            filenameWithUploadDirectory = uploadDirectory.substring(uploadDirectory.lastIndexOf(File.separator));
        }
        Path fileStorageLocation = pathProvider.getFileStorageLocation(directoryPathWithGroupName + filenameWithUploadDirectory);

        String resultFilename;
        File fullPathWithNewFile;

        if (ContentType.getExtensionTypeByFilename(fileStorageLocation.getFileName().toString()).isEmpty()) {
            resultFilename = checkUploadFilename(file);

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = fileStorageLocation.resolve(resultFilename);
            fullPathWithNewFile = new File(targetLocation.toUri());
        } else {
            resultFilename = fileStorageLocation.getFileName().toString();

            // Copy file to the target location (Replacing existing file with the same name)
//            Path targetLocation = fileStorageLocation.resolve(resultFilename);
            fullPathWithNewFile = new File(fileStorageLocation.toUri());
        }

        CompletableFuture<String> stringCompletableFuture = fileHandler.handleFile(file, fullPathWithNewFile);

        return resultFilename;
    }

    public String getFilePath(String filename, String uploadDirectory) {
        return pathProvider.getFileDownloadURI(checkFilename(filename), uploadDirectory);
    }

    public String getFilePathWithoutOrigin(String fullPathUrl) {
        return fullPathUrl.substring(fullPathUrl.indexOf("/media"));
    }
}
