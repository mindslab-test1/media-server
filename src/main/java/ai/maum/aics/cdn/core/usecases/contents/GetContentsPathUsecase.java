package ai.maum.aics.cdn.core.usecases.contents;

import ai.maum.aics.cdn.boundaries.dto.RequestGetContentPathDto;
import ai.maum.aics.cdn.boundaries.dto.RequestGetContentsPathDto;
import ai.maum.aics.cdn.boundaries.dto.ResponseFilePathDto;
import ai.maum.aics.cdn.core.usecases.MultipartFileHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class GetContentsPathUsecase {
    private final MultipartFileHelper multipartFileHelper;

    public ResponseFilePathDto execute(RequestGetContentPathDto requestGetContentPathDto) {
        String uploadDirectory = requestGetContentPathDto.getUploadDirectory();
        String filename = requestGetContentPathDto.getFilename();
        String fullPathUrl = multipartFileHelper.getFilePath(filename, uploadDirectory);
        String pathUrl = multipartFileHelper.getFilePathWithoutOrigin(fullPathUrl);
        return ResponseFilePathDto.builder()
                .fullPathUrl(fullPathUrl)
                .pathUrl(pathUrl)
                .build();
    }

    public List<ResponseFilePathDto> execute(RequestGetContentsPathDto requestGetContentsPathDto) {
        String uploadDirectory = requestGetContentsPathDto.getUploadDirectory();
        List<String> filenames = requestGetContentsPathDto.getFilenames();
        return filenames.stream()
                .map(filename -> {
                    String fullPathUrl = multipartFileHelper.getFilePath(filename, uploadDirectory);
                    String pathUrl = multipartFileHelper.getFilePathWithoutOrigin(fullPathUrl);
                    return ResponseFilePathDto.builder()
                            .fullPathUrl(fullPathUrl)
                            .pathUrl(pathUrl)
                            .build();
                })
                .collect(Collectors.toList());
    }
}
