package ai.maum.aics.cdn.core.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundRequestFileException extends BaseException {
    private final HttpStatus statusCode;
    private final String message;

    public NotFoundRequestFileException(HttpStatus statusCode, String message) {
        super(statusCode, "not_found_request_file", message);
        this.statusCode = statusCode;
        this.message = message;
    }
}
