package ai.maum.aics.cdn.core.models;

import ai.maum.aics.cdn.core.exceptions.UnsupportedContentTypeException;
import lombok.Getter;
import org.apache.commons.io.FilenameUtils;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public enum ContentType {
    VIDEO("video", Arrays.stream(FileExtension.values()).filter(fileExtension -> fileExtension.getMainType().equals("video")).collect(Collectors.toList())),
    AUDIO("audio", Arrays.stream(FileExtension.values()).filter(fileExtension -> fileExtension.getMainType().equals("audio")).collect(Collectors.toList())),
    IMAGE("image", Arrays.stream(FileExtension.values()).filter(fileExtension -> fileExtension.getMainType().equals("image")).collect(Collectors.toList())),
    TEXT("text", Arrays.stream(FileExtension.values()).filter(fileExtension -> fileExtension.getMainType().equals("text")).collect(Collectors.toList())),
    FONT("font", Arrays.stream(FileExtension.values()).filter(fileExtension -> fileExtension.getMainType().equals("font")).collect(Collectors.toList())),
    APPLICATION("application", Arrays.stream(FileExtension.values()).filter(fileExtension -> fileExtension.getMainType().equals("application")).collect(Collectors.toList()));

    private String typeName;
    private List<FileExtension> fileExtensions;

    ContentType(String typeName, List<FileExtension> fileExtensions) {
        this.typeName = typeName;
        this.fileExtensions = fileExtensions;
    }

    public static String[] getNames(Class<? extends Enum<?>> e) {
        return Arrays.stream(e.getEnumConstants()).map(anEnum -> anEnum.name().toLowerCase()).toArray(String[]::new);
    }


    public static ContentType findByFilename(String filename) {
        return Arrays.stream(ContentType.values())
                .filter(contentType -> contentType.hasExtensionType(filename))
                .findFirst()
                .orElseThrow(() -> new UnsupportedContentTypeException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Unsupported ContentType"));
    }

    public boolean hasExtensionType(String filename) {
        return fileExtensions.stream()
                .anyMatch(extension -> extension.name().equalsIgnoreCase(getExtensionTypeByFilename(filename)));
    }

    public static String getExtensionTypeByFilename(String filename) {
        return FilenameUtils.getExtension(filename);
    }
}
