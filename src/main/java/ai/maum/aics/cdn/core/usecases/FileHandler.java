package ai.maum.aics.cdn.core.usecases;

import ai.maum.aics.cdn.core.exceptions.MediaServerException;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

@Component
public class FileHandler {

    private static final Logger logger = LoggerFactory.getLogger(FileHandler.class);

    public CompletableFuture<String> handleFile(MultipartFile file, File fullPathWithNewFile) {
        try {
            FileUtils.copyInputStreamToFile(file.getInputStream(), fullPathWithNewFile);
            logger.info(Thread.currentThread().getName() + " - [Success] Upload " + file.getOriginalFilename() +
                    "(" + file.getSize() + " -> " + ai.maum.aics.cdn.util.FileUtils.readableFileSizeUnit(file.getSize()) + ")" +
                    " to " + fullPathWithNewFile.getPath());
            return CompletableFuture.completedFuture(fullPathWithNewFile.getName());
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
            try {
                FileUtils.forceDelete(fullPathWithNewFile);
            } catch (IOException e) {
                logger.error(ex.getLocalizedMessage());
                throw new MediaServerException("Could not remove file (" + e.getLocalizedMessage() + ")");
            }
            throw new MediaServerException("Could not store file (" + ex.getLocalizedMessage() + ")");
        }
    }

    public boolean handleDirectory(File directory) {
        try {
            FileUtils.forceMkdir(directory);
            return true;
        } catch (IOException ioex) {
            logger.error(ioex.getLocalizedMessage());
            return false;
        }
    }
}
