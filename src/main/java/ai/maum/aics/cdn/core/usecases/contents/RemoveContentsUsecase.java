package ai.maum.aics.cdn.core.usecases.contents;

import ai.maum.aics.cdn.boundaries.dto.RequestRemoveContentDto;
import ai.maum.aics.cdn.core.exceptions.MediaServerException;
import ai.maum.aics.cdn.infra.config.UploadFileProperties;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@RequiredArgsConstructor
@Component
public class RemoveContentsUsecase {
    private final UploadFileProperties uploadFileProperties;

    private static final Logger logger = LoggerFactory.getLogger(RemoveContentsUsecase.class);

    public boolean execute(RequestRemoveContentDto requestRemoveContentDto) {
        Path fileStorageLocation = Paths.get(uploadFileProperties.getUploadDir() + requestRemoveContentDto.getDeleteFileFullPath())
                .toAbsolutePath()
                .normalize();

        File targetFile = new File(fileStorageLocation.toUri());
        try {
            FileUtils.forceDelete(targetFile);
            return true;
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
            throw new MediaServerException("Could not remove file (" + ex.getLocalizedMessage() + ")");
        }
    }
}
