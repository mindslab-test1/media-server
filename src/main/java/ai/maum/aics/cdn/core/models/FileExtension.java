package ai.maum.aics.cdn.core.models;



public enum FileExtension {
    /* [Text] */
    HTML("text/html"),
    HTM("text/html"),
    SHTML("text/html"),

    CSS("text/css"),
    XML("text/xml"),
    MML("text/mathml"),
    TXT("text/plain"),
    JAD("text/text/vnd.sun.j2me.app-descriptor"),
    WML("text/vnd.wap.wml"),
    HTC("text/x-component"),


    /* [Image] */
    GIF("image/gif"),

    JPEG("image/jpeg"),
    JPG("image/jpeg"),

    PNG("image/png"),

    SVG("image/svg+xml"),
    SVGZ("image/svg+xml"),

    TIF("image/tiff"),
    TIFF("image/tiff"),

    WBMP("image/vnd.wap.wbmp"),
    WEBP("image/webp"),
    ICO("image/x-icon"),
    JNG("image/x-jng"),
    BMP("image/x-ms-bmp"),


    /* [Font] */
    WOFF("font/woff"),
    WOFF2("font/woff"),


    /* [Application] */
    JS("application/javascript"),
    ATOM("application/atom+xml"),
    RSS("application/rss+xml"),

    JAR("application/java-archive"),
    WAR("application/java-archive"),
    EAR("application/java-archive"),

    JSON("application/json"),
    HQX("application/mac-binhex40"),
    DOC("application/msword"),
    PDF("application/pdf"),

    PS("application/postscript"),
    EPS("application/postscript"),
    AI("application/postscript"),

    RTF("application/rtf"),
    M3U8("application/vnd.apple.mpegurl"),
    KML("application/vnd.google-earth.kml+xml"),
    KMZ("application/vnd.google-earth.kmz"),
    XLS("application/vnd.ms-excel"),
    EOT("application/vnd.ms-fontobject"),
    PPT("application/vnd.ms-powerpoint"),
    ODG("application/vnd.oasis.opendocument.graphics"),
    ODP("application/vnd.oasis.opendocument.presentation"),
    ODS("application/vnd.oasis.opendocument.spreadsheet"),
    ODT("application/vnd.oasis.opendocument.text"),
    PPTX("application/vnd.openxmlformats-officedocument.presentationml.presentation"),
    XLSX("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
    DOCX("application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
    WMLC("application/vnd.wap.wmlc"),
//    7Z("application/x-7z-compressed"),
    CCO("application/x-cocoa"),
    JARDIFF("application/x-java-archive-diff"),
    JNLP("application/x-java-jnlp-file"),
    RUN("application/x-makeself"),

    PL("application/x-perl"),
    PM("application/x-perl"),

    PRC("application/x-pilot"),
    PDB("application/x-pilot"),

    RAR("application/x-rar-compressed"),
    RPM("application/x-redhat-package-manager"),
    SEA("application/x-sea"),
    SWF("application/x-shockwave-flash"),
    SIT("application/x-stuffit"),

    TCL("application/x-tcl"),
    TK("application/x-tcl"),

    DER("application/x-x509-ca-cert"),
    PEM("application/x-x509-ca-cert"),
    CRT("application/x-x509-ca-cert"),

    XPI("application/x-xpinstall"),
    XHTML("application/xhtml+xml"),
    XSPF("application/xspf+xml"),
    ZIP("application/zip"),

    BIN("application/octet-stream"),
    EXE("application/octet-stream"),
    DLL("application/octet-stream"),
    DEB("application/octet-stream"),
    DMG("application/octet-stream"),
    ISO("application/octet-stream"),
    IMG("application/octet-stream"),
    MSI("application/octet-stream"),
    MSP("application/octet-stream"),
    MSM("application/octet-stream"),


    /* [Audio] */
    MID("audio/midi"),
    MIDI("audio/midi"),
    KAR("audio/midi"),

    MP3("audio/mpeg"),
    OGG("audio/ogg"),
    M4A("audio/x-m4a"),
    RA("audio/x-realaudio"),
    AIFF("audio/x-aiff"),
    AAC("audio/x-aac"),
    FLAC("audio/x-flac"),
    WAV("audio/wave"),
    WMA("audio/x-ms-wma"),


    /* [Video] */
//    3GPP("video/3gpp"),
//    3GP("video/3gpp"),

    TS("video/mp2t"),
    MP4("video/mp4"),

    MPEG("video/mpeg"),
    MPG("video/mpeg"),

    MOV("video/quicktime"),
    WEBM("video/webm"),
    FLV("video/x-flv"),
    M4V("video/x-m4v"),
    MNG("video/x-mng"),

    ASX("video/x-ms-asf"),
    ASF("video/x-ms-asf"),

    WMV("video/x-ms-wmv"),
    AVI("video/x-msvideo");

    private final String contentType;

    FileExtension(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return this.contentType;
    }

    public String getMainType() {
        return this.contentType.split("/")[0];
    }

    public String getSubType() {
        return this.contentType.split("/")[1];
    }
}
