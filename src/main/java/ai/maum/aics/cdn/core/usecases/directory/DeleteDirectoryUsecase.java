package ai.maum.aics.cdn.core.usecases.directory;

import ai.maum.aics.cdn.boundaries.dto.RequestDeleteDirectoryDto;
import ai.maum.aics.cdn.core.exceptions.MediaServerException;
import ai.maum.aics.cdn.core.exceptions.NotDirectoryException;
import ai.maum.aics.cdn.core.exceptions.NotExistDirectoryException;
import ai.maum.aics.cdn.infra.config.UploadFileProperties;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@RequiredArgsConstructor
@Component
public class DeleteDirectoryUsecase {
    private final UploadFileProperties uploadFileProperties;

    private static Logger logger = LoggerFactory.getLogger(DeleteDirectoryUsecase.class);

    public boolean execute(RequestDeleteDirectoryDto requestDeleteDirectoryDto) {
        String fullPath = uploadFileProperties.getUploadDir() + requestDeleteDirectoryDto.getDirectoryNameWithPath();
        File directory = new File(fullPath);

        if (!directory.exists()) {
            throw new NotExistDirectoryException(HttpStatus.BAD_REQUEST, "Not Exist directory '" + requestDeleteDirectoryDto.getDirectoryNameWithPath() + "'");
        }

        if (!directory.isDirectory()) {
            throw new NotDirectoryException(HttpStatus.BAD_REQUEST, "Not directory '" + requestDeleteDirectoryDto.getDirectoryNameWithPath() + "'");
        }

        try {
            FileUtils.forceDelete(directory);
            return true;
        } catch (IOException ex) {
            logger.error(ex.getLocalizedMessage());
            throw new MediaServerException(ex.getLocalizedMessage(), ex);
        }
    }
}
