package ai.maum.aics.cdn.core.usecases.directory;

import ai.maum.aics.cdn.boundaries.dto.RequestUpdateDirectoryDto;
import ai.maum.aics.cdn.boundaries.dto.ResponseDirectoryDto;
import ai.maum.aics.cdn.core.exceptions.*;
import ai.maum.aics.cdn.infra.config.UploadFileProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.File;

@RequiredArgsConstructor
@Component
public class UpdateDirectoryUsecase {
    private final UploadFileProperties uploadFileProperties;

    public ResponseDirectoryDto execute(String originalDirectoryName, RequestUpdateDirectoryDto updateRequestDirDto) {
        String fullPath = uploadFileProperties.getUploadDir() + updateRequestDirDto.getDirectoryPath() + File.separator + originalDirectoryName;
        File originalDirectory = new File(fullPath);
        if (!originalDirectory.exists()) {
            throw new NotExistDirectoryException(HttpStatus.BAD_REQUEST, "Not Exist directory '" + originalDirectoryName + "'");
        }

        if (!originalDirectory.isDirectory()) {
            throw new NotDirectoryException(HttpStatus.BAD_REQUEST, "Not directory '" + originalDirectoryName + "'");
        }

        File updateDirectory = new File(uploadFileProperties.getUploadDir() + updateRequestDirDto.getDirectoryPath() + updateRequestDirDto.getUpdateDirectoryName());

        boolean result = originalDirectory.renameTo(updateDirectory);
        if (!result) {
            throw new FailedDirectoryRenameException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed directory rename");
        }

        return new ResponseDirectoryDto(updateRequestDirDto.getDirectoryPath(), updateRequestDirDto.getUpdateDirectoryName());
    }
}
