package ai.maum.aics.cdn.core.exceptions;

public class MediaServerException extends RuntimeException {
    public MediaServerException(String message) {
        super(message);
    }

    public MediaServerException(String message, Throwable cause) {
        super(message, cause);
    }
}
