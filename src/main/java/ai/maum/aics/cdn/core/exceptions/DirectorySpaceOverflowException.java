package ai.maum.aics.cdn.core.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class DirectorySpaceOverflowException extends BaseException {
    private final HttpStatus statusCode;
    private final String message;

    public DirectorySpaceOverflowException(HttpStatus statusCode, String message) {
        super(statusCode, "directory_space_overflow", message);
        this.statusCode = statusCode;
        this.message = message;
    }
}
