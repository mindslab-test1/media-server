package ai.maum.aics.cdn.core.usecases.directory;

import ai.maum.aics.cdn.boundaries.dto.RequestDirectoryDto;
import ai.maum.aics.cdn.core.exceptions.AlreadyExistException;
import ai.maum.aics.cdn.core.exceptions.MediaServerException;
import ai.maum.aics.cdn.infra.config.UploadFileProperties;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@RequiredArgsConstructor
@Component
public class CreateDirectoryUsecase {
    private final UploadFileProperties uploadFileProperties;
    private static final Logger logger = LoggerFactory.getLogger(CreateDirectoryUsecase.class);

    public boolean execute(RequestDirectoryDto requestDirDto) {
        String fullPath = uploadFileProperties.getUploadDir() + requestDirDto.getDirectoryName();
        File directory = new File(fullPath);
        if (directory.exists()) {
            throw new AlreadyExistException(HttpStatus.BAD_REQUEST, "This directory already exists");
        }

        try {
            FileUtils.forceMkdir(directory);
            return true;
        } catch (IOException ioex) {
            logger.error(ioex.getLocalizedMessage());
            throw new MediaServerException(ioex.getMessage(), ioex);
        }
    }
}
