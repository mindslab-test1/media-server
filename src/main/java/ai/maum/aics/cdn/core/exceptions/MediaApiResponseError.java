package ai.maum.aics.cdn.core.exceptions;

import lombok.*;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@Builder
public class MediaApiResponseError {
    private int statusCode;
    private String code;
    private String message;
}
