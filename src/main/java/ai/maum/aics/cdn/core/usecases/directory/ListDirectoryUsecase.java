package ai.maum.aics.cdn.core.usecases.directory;

import ai.maum.aics.cdn.boundaries.dto.ResponseFileInfoDto;
import ai.maum.aics.cdn.infra.config.UploadFileProperties;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class ListDirectoryUsecase {
    private final UploadFileProperties uploadFileProperties;

    private static final Logger logger = LoggerFactory.getLogger(ListDirectoryUsecase.class);

    public List<ResponseFileInfoDto> execute(String path) {
        String fullPath = uploadFileProperties.getUploadDir() + path;
        File directory = new File(fullPath);
        List<File> fileList = Arrays.asList(directory.listFiles());
        return fileList.stream()
                .map(file ->
                        ResponseFileInfoDto.builder()
                                .name(file.getName())
                                .path(getFilePath(file))
                                .size(file.length())
                                .isDirectory(file.isDirectory())
                                .isFile(file.isFile())
                                .parentPath(getParentPath(file))
                                .build()
                ).collect(Collectors.toList());
    }

    private String excludeUploadDir(String filePath) {
        return filePath.replace(uploadFileProperties.getUploadDir(), "");
    }

    private String getFilePath(File file) {
        String filePathUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/media" + excludeUploadDir(file.getPath()))
                .toUriString();
        String decodedUri;
        try {
            decodedUri = URLDecoder.decode(filePathUri, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getLocalizedMessage());
            decodedUri = filePathUri;
        }

        return file.isFile() ?
                decodedUri :
                excludeUploadDir(file.getPath());
    }

    private String getParentPath(File file) {
        return file.getParent().equalsIgnoreCase(uploadFileProperties.getUploadDir()) ?
                File.separator :
                excludeUploadDir(file.getParent());
    }
}
