package ai.maum.aics.cdn.core.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class MismatchedRequestInputException extends BaseException {
    private final HttpStatus statusCode;
    private final String message;

    public MismatchedRequestInputException(HttpStatus statusCode, String message) {
        super(statusCode, "mismatched_request_input", message);
        this.statusCode = statusCode;
        this.message = message;
    }
}