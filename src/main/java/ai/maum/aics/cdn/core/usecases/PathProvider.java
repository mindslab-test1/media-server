package ai.maum.aics.cdn.core.usecases;

import ai.maum.aics.cdn.core.exceptions.MismatchedRequestInputException;
import ai.maum.aics.cdn.core.models.ContentType;
import ai.maum.aics.cdn.infra.config.UploadFileProperties;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

@RequiredArgsConstructor
@Component
public class PathProvider {
    private final UploadFileProperties uploadFileProperties;
    private final FileHandler fileHandler;

    private static final Logger logger = LoggerFactory.getLogger(PathProvider.class);

    public Path getFileStorageLocation(String directoryPath) {
        String checkedDirectorySeparatorPath = (directoryPath.startsWith("/"))? directoryPath : File.separator + directoryPath;
        Path uploadDirectoryPath = Paths.get(uploadFileProperties.getUploadDir() + checkedDirectorySeparatorPath)
                .toAbsolutePath()
                .normalize();

        if (!FilenameUtils.getExtension(uploadDirectoryPath.getFileName().toString()).isEmpty()) {
            return uploadDirectoryPath;
        }

        File uploadDirectoryFile = uploadDirectoryPath.toFile();

        if (!uploadDirectoryFile.exists()) {
            fileHandler.handleDirectory(uploadDirectoryFile);
        }
        return uploadDirectoryPath;
    }

    public String getFileDownloadURI(String notDuplicatedFilename, String uploadDirectory) {
        String directoryPath = getUploadDirectoryWithGroupName(notDuplicatedFilename, uploadDirectory);
        getFileStorageLocation(directoryPath);
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/media" + directoryPath)
                .path("/" + notDuplicatedFilename)
                .toUriString();
        try {
            return URLDecoder.decode(fileDownloadUri, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getLocalizedMessage());
            return fileDownloadUri;
        }
    }

    public String getUploadDirectoryWithGroupName(String filenameWithExtension, String uploadDirectory) {
        String contentTypeGroupName = ContentType.findByFilename(filenameWithExtension).getTypeName();
        if (ContentType.getExtensionTypeByFilename(uploadDirectory).isEmpty()) {
            String groupNameInUploadDirectory = uploadDirectory.substring(uploadDirectory.lastIndexOf(File.separator) + 1);
            if (Arrays.asList(ContentType.getNames(ContentType.class)).contains(groupNameInUploadDirectory)) {
                if (!groupNameInUploadDirectory.equalsIgnoreCase(contentTypeGroupName)) {
                    throw new MismatchedRequestInputException(HttpStatus.BAD_REQUEST, "Request input(" + FilenameUtils.getExtension(filenameWithExtension) + ", " + uploadDirectory + ") is mismatched");
                }
                return uploadDirectory;
            } else {
                return uploadDirectory + File.separator + contentTypeGroupName;
            }
        } else {
            String filenameInUploadDirectory = uploadDirectory.substring(uploadDirectory.lastIndexOf(File.separator) + 1);
            String uploadDirectoryWithoutFilename = uploadDirectory.substring(0, uploadDirectory.indexOf(File.separator + filenameInUploadDirectory));
            String groupNameInUploadDirectory = uploadDirectoryWithoutFilename.substring(uploadDirectoryWithoutFilename.lastIndexOf(File.separator) + 1);
            // reqInput = /aics/fanmeet/audio/asdf.wav
            if (Arrays.asList(ContentType.getNames(ContentType.class)).contains(groupNameInUploadDirectory)) {
                if (!groupNameInUploadDirectory.equalsIgnoreCase(contentTypeGroupName)) {
                    throw new MismatchedRequestInputException(HttpStatus.BAD_REQUEST, "Request input(" + FilenameUtils.getExtension(filenameWithExtension) + ", " + uploadDirectory + ") is mismatched");
                }
                return uploadDirectoryWithoutFilename;
            } else { // reqInput = /aics/fanmeet/asdf.wav
                return uploadDirectoryWithoutFilename + File.separator + contentTypeGroupName;
            }
        }
    }
}
