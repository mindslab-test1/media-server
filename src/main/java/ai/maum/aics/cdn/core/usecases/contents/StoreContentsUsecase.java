package ai.maum.aics.cdn.core.usecases.contents;

import ai.maum.aics.cdn.boundaries.dto.RequestUploadDirectoryDto;
import ai.maum.aics.cdn.boundaries.dto.RequestUploadFilesDirectoryDto;
import ai.maum.aics.cdn.boundaries.dto.ResponseUploadedFileInfo;
import ai.maum.aics.cdn.core.usecases.MultipartFileHelper;
import ai.maum.aics.cdn.core.usecases.PathProvider;
import ai.maum.aics.cdn.infra.aop.CheckUploadSpace;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class StoreContentsUsecase {
    private final MultipartFileHelper multipartFileHelper;
    private final PathProvider pathProvider;


    private static final Logger logger = LoggerFactory.getLogger(StoreContentsUsecase.class);

    @CheckUploadSpace // method call 전,후에 업로드공간 체크
    public ResponseUploadedFileInfo execute(RequestUploadDirectoryDto requestUploadDirectoryDto) {
        MultipartFile file = requestUploadDirectoryDto.getFile();
        String uploadDirectory = requestUploadDirectoryDto.getUploadDirectory();

        String filename = multipartFileHelper.createFile(file, uploadDirectory);

        String uploadDirectoryWithoutFilename = (uploadDirectory.contains(filename)) ?
                uploadDirectory.replace(File.separator + filename, "") : uploadDirectory;

        String fileDownloadUri = pathProvider.getFileDownloadURI(filename, uploadDirectoryWithoutFilename);

        if (logger.isDebugEnabled()) {
            logger.debug("test single upload");
        }

        return new ResponseUploadedFileInfo(filename, fileDownloadUri, file.getContentType(), file.getSize());
    }

    @CheckUploadSpace
    public List<ResponseUploadedFileInfo> execute(RequestUploadFilesDirectoryDto requestUploadFilesDirectoryDto) {
        List<MultipartFile> files = requestUploadFilesDirectoryDto.getFiles();
        String uploadDirectory = requestUploadFilesDirectoryDto.getUploadDirectory();

        if (logger.isDebugEnabled()) {
            logger.debug("test multiple upload");
        }

        return files.stream().map(file -> {
            String filename = multipartFileHelper.createFile(file, uploadDirectory);

            String uploadDirectoryWithoutFilename = (uploadDirectory.contains(filename)) ?
                    uploadDirectory.replace(File.separator + filename, "") : uploadDirectory;

            String fileDownloadUri = pathProvider.getFileDownloadURI(filename, uploadDirectoryWithoutFilename);

            return new ResponseUploadedFileInfo(filename, fileDownloadUri, file.getContentType(), file.getSize());
        }).collect(Collectors.toList());
    }

}
