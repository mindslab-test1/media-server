package ai.maum.aics.cdn.core.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidParamRequestException extends BaseException {
    private final HttpStatus statusCode;
    private final String message;

    public InvalidParamRequestException(HttpStatus statusCode, String message) {
        super(statusCode, "invalid_param_request", message);
        this.statusCode = statusCode;
        this.message = message;
    }
}
