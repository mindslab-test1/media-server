package ai.maum.aics.cdn.core.usecases.contents;

import ai.maum.aics.cdn.core.exceptions.NotFoundRequestFileException;
import ai.maum.aics.cdn.core.exceptions.UnsupportedContentTypeException;
import ai.maum.aics.cdn.core.models.ContentType;
import ai.maum.aics.cdn.core.usecases.PathProvider;
import ai.maum.aics.cdn.infra.config.UploadFileProperties;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.support.ResourceRegion;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRange;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

@RequiredArgsConstructor
@Component
public class LoadContentsUsecase {
    private final PathProvider pathProvider;
    private final UploadFileProperties uploadFileProperties;

    private static final Logger logger = LoggerFactory.getLogger(LoadContentsUsecase.class);

    public Pair<Resource, ResourceRegion> execute(String dirPath, String filenameWithExtension, HttpHeaders httpHeaders) throws IOException {
        Path fileStorageLocation = pathProvider.getFileStorageLocation(dirPath);
        Path filePath = fileStorageLocation.resolve(filenameWithExtension).normalize();
        Resource urlResource = new UrlResource(filePath.toUri());
        if(!urlResource.exists()) {
            throw new NotFoundRequestFileException(HttpStatus.NOT_FOUND, "File not found " + filePath);
        }

        switch (ContentType.findByFilename(filenameWithExtension)) {
            case IMAGE:
            case APPLICATION:
            case TEXT:
            case FONT:
                return new ImmutablePair<>(urlResource, null);
            case AUDIO:
            case VIDEO:
                ResourceRegion resourceRegion = loadStreamingFile(urlResource, httpHeaders);
                return new ImmutablePair<>(resourceRegion.getResource(), resourceRegion);
            default:
                throw new UnsupportedContentTypeException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "Unsupported ContentType");
        }
    }

    private ResourceRegion loadStreamingFile(Resource urlResource, HttpHeaders httpHeaders) throws IOException {
        long chunkSize = uploadFileProperties.getChunkSize()*1024L;
        long contentLength = urlResource.contentLength();

        Optional<HttpRange> rangeOptional = httpHeaders.getRange().stream().findFirst();
        if(rangeOptional.isPresent()) { // 분할 요청인 경우
            HttpRange httpRange = rangeOptional.get();
            long start = httpRange.getRangeStart(contentLength);
            long end = httpRange.getRangeEnd(contentLength);
            long rangeLength = Long.min(chunkSize, end - start + 1);

            if (logger.isDebugEnabled()) {
                logger.debug("Request HttpRange - " + urlResource.getFilename());
            }

            return new ResourceRegion(urlResource, start, rangeLength);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Request Full HttpRange - " + urlResource.getFilename());
        }

        long rangeLength = Long.min(chunkSize, contentLength);
        return new ResourceRegion(urlResource, 0 , rangeLength);
    }

}
