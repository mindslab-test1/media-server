package ai.maum.aics.cdn.core.usecases.directory;

import ai.maum.aics.cdn.boundaries.dto.RequestDirectoryDto;
import ai.maum.aics.cdn.core.exceptions.AlreadyExistException;
import ai.maum.aics.cdn.core.exceptions.MediaServerException;
import ai.maum.aics.cdn.infra.config.UploadFileProperties;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;


@RunWith(PowerMockRunner.class)
@PrepareForTest(FileUtils.class)
class CreateDirectoryUsecaseTest {

    private UploadFileProperties mockUploadFileProperties = Mockito.mock(UploadFileProperties.class);

    private CreateDirectoryUsecase createDirectoryUsecase = new CreateDirectoryUsecase(mockUploadFileProperties);

    @Before
    public void setup() {
        PowerMockito.mockStatic(FileUtils.class);
    }

    @Test
    public void testMockSetup() {
        assertThat(createDirectoryUsecase).isNotNull();
    }

    @Test
    public void testAlreadyExistDirectory() throws Exception {
        // given
        RequestDirectoryDto requestDirectoryDto = new RequestDirectoryDto();
        requestDirectoryDto.setDirectoryName("/media");

        given(mockUploadFileProperties.getUploadDir()).willReturn("/home/junsu");

        // when, then
        assertThatThrownBy(() -> createDirectoryUsecase.execute(requestDirectoryDto))
                .isInstanceOf(AlreadyExistException.class)
                .hasMessageContaining("This directory already exists");
    }

    @Test
    @Ignore
    public void testFailedDirectoryCreate() throws Exception {
        // given
        RequestDirectoryDto requestDirectoryDto = new RequestDirectoryDto();
        requestDirectoryDto.setDirectoryName("/temp");

        given(mockUploadFileProperties.getUploadDir()).willReturn("/home/junsu/media");

        // 호환 안됌. Test Wrapper class 필요
        PowerMockito.mockStatic(FileUtils.class);
        PowerMockito.doThrow(new IOException()).when(FileUtils.class);

        // when, then
        assertThatThrownBy(() -> createDirectoryUsecase.execute(requestDirectoryDto))
                .isInstanceOf(MediaServerException.class);
    }
}
