pipeline {

    agent any

    environment {
        // container name
        projectName = "media-server"

        // local(test)
        localServerIp = "192.168.35.50"
        localFileStorageVolume = "/media/nas:/DATA/new_content"
        localLogVolume = "/home/meloning/media-server/logs:/logs"
        localPort = "8950:8950"

        // canary
        canaryServerIp = "114.108.173.114"
        canaryFileStorageVolume = "/DATA/new_content:/DATA/new_content"
        canaryLogVolume = "/home/minds/media-server/logs:/logs"
        canaryPort = "8950"
        canaryProtocol = "https"
        canaryDomain = "aics-test.maum.ai"

        // master(prod)
        prodServerIp = "114.108.173.102"
        prodFileStorageVolume = "/DATA/media:/DATA/media"
        prodLogVolume = "/home/minds/media-server/logs:/logs"
        prodPort = "8950"
        prodProtocol = "https"
        prodDomain = "file.sangsangkids.co.kr"

        MINDS_SERVER_SSH_PASSWORD = "akdlswm1234!"
        slack_channel = "#jenkins-ci"
    }
    stages {
        stage('Environment') {
            parallel {
                stage('chmod') {
                    steps {
                        sh 'chmod 755 gradlew'
                        sh 'chmod +x ./scripts/*'
                    }
                }
                stage('display') {
                    steps {
                        sh 'printenv'
                        sh 'pwd'
                        sh 'ls -al'
                        sh 'echo "${MINDS_SERVER_SSH_PASSWORD}"'
                    }
                }
            }
            post {
                success {
                    script {
                        env.GIT_COMMIT = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
                        echo 'env.GIT_COMMIT : ${env.GIT_COMMIT}'
                    }
                }
            }
        }
        stage('Build Source') {
            steps {
                sh './gradlew clean build'
            }
        }
        stage('Test Source') {
            steps {
                sh './gradlew test'
            }
        }
        stage('Build docker image') {
            steps {
                script {
                    sh "docker build -t ${MINDS_DOCKER_REGISTRY}/$projectName:${env.GIT_COMMIT} ."
                }
            }
            post {
                always {
                    sh "docker tag ${MINDS_DOCKER_REGISTRY}/$projectName:${env.GIT_COMMIT} ${MINDS_DOCKER_REGISTRY}/$projectName:latest"
                }
                failure {
                    slackSend (channel: '#jenkins-ci', color: '#FF0000', message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.RUN_DISPLAY_URL})")
                }
            }
        }
        stage('Push docker image') {
            steps {
                sh "docker push ${MINDS_DOCKER_REGISTRY}/$projectName:${env.GIT_COMMIT}"
                sh "docker push ${MINDS_DOCKER_REGISTRY}/$projectName:latest"
            }
            post {
                always {
                    sh "docker rmi ${MINDS_DOCKER_REGISTRY}/$projectName:${env.GIT_COMMIT}"
                    sh "docker rmi ${MINDS_DOCKER_REGISTRY}/$projectName:latest"
                }
            }
        }
        stage('Deploy docker container at canary') {
            when { branch 'canary' }
            steps {
                sh "sshpass -p ${MINDS_SERVER_SSH_PASSWORD} scp -o StrictHostKeyChecking=no -r ./scripts/* ${MINDS_GENERAL_SERVER_USER}@$canaryServerIp:/home/${MINDS_GENERAL_SERVER_USER}/media-server/scripts"
                sh "sshpass -p ${MINDS_SERVER_SSH_PASSWORD} ssh -o StrictHostKeyChecking=no -t -t ${MINDS_GENERAL_SERVER_USER}@$canaryServerIp /home/${MINDS_GENERAL_SERVER_USER}/media-server/scripts/./stop.sh ${canaryProtocol} ${canaryDomain}"
                sh "sshpass -p ${MINDS_SERVER_SSH_PASSWORD} ssh -o StrictHostKeyChecking=no -t -t ${MINDS_GENERAL_SERVER_USER}@$canaryServerIp /home/${MINDS_GENERAL_SERVER_USER}/media-server/scripts/./start.sh ${canaryProtocol} ${canaryDomain} ${MINDS_DOCKER_REGISTRY} ${canaryFileStorageVolume} ${canaryLogVolume} ${env.GIT_COMMIT} canary"
                sh "sshpass -p ${MINDS_SERVER_SSH_PASSWORD} ssh -o StrictHostKeyChecking=no -t -t ${MINDS_GENERAL_SERVER_USER}@$canaryServerIp /home/${MINDS_GENERAL_SERVER_USER}/media-server/scripts/./health.sh ${canaryProtocol} ${canaryDomain}"
            }
        }
        stage('Deploy docker container at prod') {
            when { branch 'master' }
            steps {
                sh "sshpass -p ${MINDS_SERVER_SSH_PASSWORD} scp -o StrictHostKeyChecking=no -r ./scripts/* ${MINDS_GENERAL_SERVER_USER}@$prodServerIp:/home/${MINDS_GENERAL_SERVER_USER}/media-server/scripts"
                sh "sshpass -p ${MINDS_SERVER_SSH_PASSWORD} ssh -o StrictHostKeyChecking=no -t -t ${MINDS_GENERAL_SERVER_USER}@$prodServerIp /home/${MINDS_GENERAL_SERVER_USER}/media-server/scripts/./stop.sh ${prodProtocol} ${prodDomain}"
                sh "sshpass -p ${MINDS_SERVER_SSH_PASSWORD} ssh -o StrictHostKeyChecking=no -t -t ${MINDS_GENERAL_SERVER_USER}@$prodServerIp /home/${MINDS_GENERAL_SERVER_USER}/media-server/scripts/./start.sh ${prodProtocol} ${prodDomain} ${MINDS_DOCKER_REGISTRY} ${prodFileStorageVolume} ${prodLogVolume} ${env.GIT_COMMIT} prod"
                sh "sshpass -p ${MINDS_SERVER_SSH_PASSWORD} ssh -o StrictHostKeyChecking=no -t -t ${MINDS_GENERAL_SERVER_USER}@$prodServerIp /home/${MINDS_GENERAL_SERVER_USER}/media-server/scripts/./health.sh ${prodProtocol} ${prodDomain}"
            }
        }
    }
    post {
        success {
            slackSend (channel: '#jenkins-ci', color: '#00FF00', message: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.RUN_DISPLAY_URL})")
        }
        failure {
            slackSend (channel: '#jenkins-ci', color: '#FF0000', message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.RUN_DISPLAY_URL})")
        }
    }
}