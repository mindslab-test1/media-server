FROM adoptopenjdk:8-jdk-hotspot AS builder
COPY gradlew .
COPY gradle gradle
COPY build.gradle .
COPY settings.gradle .
COPY src src
RUN chmod +x ./gradlew
RUN ./gradlew bootJar

FROM adoptopenjdk:8-jdk-hotspot
COPY --from=builder build/libs/*.jar media-server.jar
RUN mkdir -p /home/junsu/media /DATA/new_content /DATA/media

ARG ENVIRONMENT=prod

ENV SPRING_PROFILES_ACTIVE=${ENVIRONMENT}

EXPOSE 8950
ENTRYPOINT ["java","-jar","/media-server.jar"]
